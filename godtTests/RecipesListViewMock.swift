//
//  RecipesListViewMock.swift
//  godt
//
//  Created by Maciej Baginski on 26/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation
@testable import godt

class RecipesListViewMock: RecipesListView {
    var showAlertCalled = false
    var updateDataCalled = false
    var startLoadingCalled = false
    var stopLoadingCalled = false
    
    func updateData(){
        updateDataCalled = true
    }
    func startLoading(){
        startLoadingCalled = true
    }
    func stopLoading(){
        stopLoadingCalled = true
    }
    func showAlert(title: String, message: String, buttonText: String){
        showAlertCalled = true
    }
}
