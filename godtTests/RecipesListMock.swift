//
//  RecipesListMock.swift
//  godt
//
//  Created by Maciej Baginski on 26/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation
@testable import godt

class RecipesListRouterMock: RecipesListRouter {
    var showRecipeDetailsViewCalled = false
    
    func showRecipeDetailsView(recipe: RecipeViewData){
        showRecipeDetailsViewCalled = true
    }
}
