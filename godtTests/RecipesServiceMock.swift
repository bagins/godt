//
//  RecipesServiceMock.swift
//  godt
//
//  Created by Maciej Baginski on 26/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation
@testable import godt

class RecipesServiceMock: RecipesService {
    var getRecipesCalled = false
    
    private let recipes: [Recipe]
    
    init(recipes: [Recipe]) {
        self.recipes = recipes
    }
    
    func getRecipes(completion:@escaping (Error?, [Recipe]?) -> Void){
        getRecipesCalled = true
        if recipes.count > 0{
            completion(nil, recipes)
        }
        else {
            completion(NSError(domain: "", code: 0, userInfo: nil), nil)
        }
    }
}
