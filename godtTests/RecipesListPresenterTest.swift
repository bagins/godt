//
//  RecipesListPresenterTest.swift
//  godt
//
//  Created by Maciej Baginski on 26/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import XCTest
@testable import godt

class RecipesListPresenterTest: XCTestCase {
    
    var recipesServiceMock = RecipesServiceMock(recipes: Recipe.createRecipes())
    var noResultsRecipesServiceMock = RecipesServiceMock(recipes: [])
    
    var persistentDataServiceMock = PersistentDataServiceMock()
    
    var recipesListRouterMock = RecipesListRouterMock()
    
    var recipesListViewMock = RecipesListViewMock()
    
    override func setUp() {
        super.setUp()
        recipesServiceMock = RecipesServiceMock(recipes: Recipe.createRecipes())
        noResultsRecipesServiceMock = RecipesServiceMock(recipes: [])
        
        persistentDataServiceMock = PersistentDataServiceMock()
        
        recipesListRouterMock = RecipesListRouterMock()
        
        recipesListViewMock = RecipesListViewMock()
    }
    
    
    func testRoutingToDetails() {
        //given
        let presenter = RecipesListPresenter(view: recipesListViewMock, recipesService: recipesServiceMock, persistentDataService: persistentDataServiceMock, router: recipesListRouterMock)
        
        //when
        presenter.getRecipes()
        presenter.selectRecipe(atIndex: 0)
        
        //then
        XCTAssertTrue(recipesListRouterMock.showRecipeDetailsViewCalled)
    }
    
    func testLoadingData() {
        //given
        let presenter = RecipesListPresenter(view: recipesListViewMock, recipesService: recipesServiceMock, persistentDataService: persistentDataServiceMock, router: recipesListRouterMock)
        
        //when
        presenter.getRecipes()
        
        //then
        XCTAssertTrue(recipesListViewMock.startLoadingCalled)
        XCTAssertTrue(recipesListViewMock.stopLoadingCalled)
        XCTAssertTrue(recipesListViewMock.updateDataCalled)
        XCTAssertFalse(recipesListViewMock.showAlertCalled)
    }
    
    func testFailedLoadingData() {
        //given
        let presenter = RecipesListPresenter(view: recipesListViewMock, recipesService: noResultsRecipesServiceMock, persistentDataService: persistentDataServiceMock, router: recipesListRouterMock)
        
        //when
        presenter.getRecipes()
        
        //then
        XCTAssertTrue(recipesListViewMock.startLoadingCalled)
        XCTAssertTrue(recipesListViewMock.stopLoadingCalled)
        XCTAssertFalse(recipesListViewMock.updateDataCalled)
        XCTAssertTrue(recipesListViewMock.showAlertCalled)
    }

    
}
