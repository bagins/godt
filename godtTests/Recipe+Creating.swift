//
//  Recipe+Creating.swift
//  godt
//
//  Created by Maciej Baginski on 26/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation
@testable import godt

extension Recipe {
    static func createRecipes() -> [Recipe]{
        return [
            Recipe(title: "TestTitle1", description: "test description 1", ingredients: nil, imageUrl: nil),
            Recipe(title: "TestTitle1", description: "test description 1", ingredients: nil, imageUrl: nil),
            Recipe(title: "TestTitle1", description: "test description 1", ingredients: nil, imageUrl: nil),
            Recipe(title: "TestTitle1", description: "test description 1", ingredients: nil, imageUrl: nil),
        ]
    }
}
