//
//  PersistentDataServiceMock.swift
//  godt
//
//  Created by Maciej Baginski on 26/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation
@testable import godt

class PersistentDataServiceMock: PersistentDataService {
    var archiveDataCalled = false
    var unarchiveDataDataCalled = false

    
    func archiveData(data: Any, fileName: String, completionBlock: (Error?) -> Void){
        archiveDataCalled = true
        completionBlock(NSError(domain: "", code: 0, userInfo: nil))
    }
    func unarchiveData(fileName: String, completionBlock: (Error?, Any?) -> Void){
        unarchiveDataDataCalled = true
        completionBlock(NSError(domain: "", code: 0, userInfo: nil), nil)
    }
}
