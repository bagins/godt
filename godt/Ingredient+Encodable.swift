//
//  Ingredient+Encodable.swift
//  godt
//
//  Created by Maciej Baginski on 23/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation

extension Ingredient: Encodable, Decodable {

    init(dictionary: [String: Any]) {
        amount = dictionary["amount"] as? Int
        hint = dictionary["hint"] as? String
        name = dictionary["name"] as? String
        symbol = dictionary["symbol"] as? String
    }

    func encode() -> [String: Any] {
        var result = [String: Any]()
        result["amount"] = amount
        result["hint"] = hint
        result["name"] = name
        result["symbol"] = symbol
        return result
    }
}
