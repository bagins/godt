//
//  RecipesListRouter.swift
//  godt
//
//  Created by Maciej Baginski on 26/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation
import UIKit

protocol RecipesListRouter: Router {
    func showRecipeDetailsView(recipe: RecipeViewData)
}

class RecipesListRouterImpl: RecipesListRouter {
    private weak var recipesListViewController: RecipesListViewController?
    private var recipe: RecipeViewData?

    init(controller: RecipesListViewController) {
        recipesListViewController = controller
    }

    func showRecipeDetailsView(recipe: RecipeViewData) {
        self.recipe = recipe
        recipesListViewController?.performSegue(withIdentifier: "showRecipeDetails", sender: nil)
    }

    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailsController = segue.destination as? RecipeDetailsViewController {
            guard let recipe = recipe else {
                return
            }
            let presenter = RecipeDetailsPresenter(view: detailsController, recipeViewData: recipe)
            detailsController.presenter = presenter
        }
    }
}
