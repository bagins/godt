//
//  RecipesListView.swift
//  godt
//
//  Created by Maciej Baginski on 23/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation

protocol RecipesListView: class {
    func updateData()
    func startLoading()
    func stopLoading()
    func showAlert(title: String, message: String, buttonText: String)
}
