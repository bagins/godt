//
//  RecipesListPresenter.swift
//  godt
//
//  Created by Maciej Baginski on 23/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation

class RecipesListPresenter {

    enum SearchScope: Int {
        case title
        case description

        var text: String {
            switch self {
            case .title:
                return "recipes_list_title".localized
            case .description:
                return "recipes_list_description".localized
            }
        }
    }

    private let recipesFileName = "recipes"

    private let recipesService: RecipesService
    private let persistentDataService: PersistentDataService
    var router: RecipesListRouter

    weak private var view: RecipesListView?

    private var recipesViewData: [RecipeViewData] = []
    private var activeRecipesViewData: [RecipeViewData] = []
    private var selectedRecipeIndex = 0

    init(view: RecipesListView, recipesService: RecipesService, persistentDataService: PersistentDataService, router: RecipesListRouter) {
        self.view = view
        self.recipesService = recipesService
        self.persistentDataService = persistentDataService
        self.router = router
    }

    func viewDidLoad() {
        getRecipes()
    }

    func getRecipesSearchOptions() -> [String] {
        return [SearchScope(rawValue: 0)?.text ?? "", SearchScope(rawValue: 1)?.text ?? ""]
    }

    func selectRecipe(atIndex index: Int) {
        selectedRecipeIndex = index
        router.showRecipeDetailsView(recipe: activeRecipesViewData[index])
    }

    var numberOfRecipes: Int {
        return activeRecipesViewData.count
    }

    func getRecipe(atIndex index: Int) -> RecipeViewData {
        return activeRecipesViewData[index]
    }

    func getRecipes() {
        view?.startLoading()
        recipesService.getRecipes { [weak self] (_, data) in
            self?.view?.stopLoading()
            if let data = data {
                if let recipesFileName = self?.recipesFileName {
                    let dataToArchive = data.map {
                        $0.encode()
                    }
                    self?.persistentDataService.archiveData(data: dataToArchive, fileName: recipesFileName) { _ in
                        self?.setNewRecipes(recipes: data)
                    }
                }
            } else {
                guard let recipesFileName = self?.recipesFileName else {
                    return
                }
                self?.persistentDataService.unarchiveData(fileName: recipesFileName, completionBlock: { (_, data) in
                    if let data = data as? [[String: Any]] {
                        let recipes = data.map {
                            Recipe(dictionary: $0)
                        }
                        self?.setNewRecipes(recipes: recipes)
                    } else {
                        let title = "error_no_data_available_title".localized
                        let message = "error_no_data_available_message".localized
                        let buttonText = "ok_button".localized
                        self?.view?.showAlert(title: title, message: message, buttonText: buttonText)
                    }
                })
            }
        }
    }

    func filterRecipes(searchText: String?, scopeIndex: Int) {
        if let searchText = searchText, searchText.characters.count != 0 {
            activeRecipesViewData = recipesViewData.filter { viewData -> Bool in
                var textToSearchIn = ""
                if scopeIndex == 0 {
                    textToSearchIn = viewData.title
                } else {
                    textToSearchIn = viewData.description
                }
                let rangeOfSearchedText = textToSearchIn.range(of: searchText, options: .caseInsensitive)
                return rangeOfSearchedText != nil
            }
        } else {
            activeRecipesViewData = recipesViewData
        }
        view?.updateData()
    }

    private func createRecipeViewData(recipes: [Recipe]) -> [RecipeViewData] {
        let recipesViewData = recipes.map { recipe -> RecipeViewData in
            var ingredientsText = ""
            if let ingredients = recipe.ingredients {
                ingredientsText = "recipe_details_ingredients".localized + ": "
                let lastElementIndex = ingredients.count - 1
                for (index, ingredient) in ingredients.enumerated() {
                    guard let name = ingredient.name else {
                        continue
                    }
                    if index == lastElementIndex {
                        ingredientsText += name
                    } else {
                        ingredientsText += "\(name), "
                    }
                }
            }
            let title = recipe.title ?? ""

            let regularExpression = "<[^>]+>"
            let description = recipe.description?.replacingOccurrences(of: regularExpression, with: "", options: .regularExpression, range: nil) ?? ""

            return RecipeViewData(title: title, description: description, ingredients: ingredientsText, imageUrl: recipe.imageUrl)
        }
        return recipesViewData
    }

    private func setNewRecipes(recipes: [Recipe]) {
        recipesViewData = createRecipeViewData(recipes: recipes)
        activeRecipesViewData = recipesViewData
        view?.updateData()
    }
}
