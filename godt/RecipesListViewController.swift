//
//  ViewController.swift
//  godt
//
//  Created by Maciej Baginski on 23/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import UIKit
import Kingfisher

class RecipesListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    fileprivate var searchController = UISearchController(searchResultsController: nil)
    fileprivate let refreshControl = UIRefreshControl()

    fileprivate let cellWidthToHeightRatio = CGFloat(3.5)
    fileprivate let leftCellId = "leftCellId"
    fileprivate let rightCellId = "rightCellId"

    fileprivate var presenter: RecipesListPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()

        let router = RecipesListRouterImpl(controller: self)
        let recipesService = RecipesServiceImpl()
        let dataService = PersistentDataServiceImpl()
        self.presenter = RecipesListPresenter(view: self, recipesService: recipesService, persistentDataService: dataService, router: router)

        self.definesPresentationContext = true
        searchController.searchBar.scopeButtonTitles = presenter.getRecipesSearchOptions()
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.backgroundColor = .clear
        searchController.searchBar.tintColor = .godtBrown
        searchController.searchBar.barTintColor = .godtBackground
        searchController.searchBar.layer.borderWidth = 1
        searchController.searchBar.layer.borderColor = UIColor.godtBackground.cgColor
        searchController.searchBar.sizeToFit()
        searchController.searchBar.delegate = self
        tableView.tableHeaderView = searchController.searchBar
        tableView.backgroundView = UIView()
        tableView.prefetchDataSource = self

        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)

        view.backgroundColor = .godtBackground
        navigationItem.title = "godt.no"
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.godtBrown]
        navigationController?.navigationBar.backgroundColor = .godtBackground
        navigationController?.navigationBar.tintColor = .godtBrown

        presenter.viewDidLoad()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        presenter.router.prepare(for: segue, sender: sender)
    }

    func refreshData() {
        presenter.getRecipes()
    }
}

extension RecipesListViewController: UITableViewDelegate, UITableViewDataSource, UITableViewDataSourcePrefetching {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRecipes
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = indexPath.row % 2 == 0 ? leftCellId : rightCellId
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as? RecipeTableViewCell else {
            fatalError("failed to dequeue cell at \(self.description)")
        }
        let recipe = presenter.getRecipe(atIndex: indexPath.row)
        cell.setupWithRecipeDetails(recipeDetails: recipe)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.selectRecipe(atIndex: indexPath.row)
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? RecipeTableViewCell {
            cell.cancelDownloadTasks()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.width / cellWidthToHeightRatio
    }

    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        var urlsToPrefetch = [URL]()
        indexPaths.map {
            $0.row
        }.forEach { row in
            let recipe = presenter.getRecipe(atIndex: row)
            if let url = recipe.imageUrl {
                urlsToPrefetch.append(url)
            }
        }
        ImagePrefetcher(urls: urlsToPrefetch).start()
    }
}

extension RecipesListViewController: RecipesListView {

    func startLoading() {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
            self.activityIndicator.isHidden = false
        }
    }

    func stopLoading() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
            self.refreshControl.endRefreshing()
        }
    }

    func updateData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    func showAlert(title: String, message: String, buttonText: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonText, style: .default, handler: { _ in
            self.presenter.getRecipes()
        }))
        alert.view.tintColor = .godtBrown
        present(alert, animated: true, completion: nil)
    }
}

extension RecipesListViewController: UISearchResultsUpdating, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        presenter.filterRecipes(searchText: searchController.searchBar.text, scopeIndex: searchController.searchBar.selectedScopeButtonIndex)
    }

    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        updateSearchResults(for: searchController)
    }
}
