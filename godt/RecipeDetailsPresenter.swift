//
//  RecipeDetailsPresenter.swift
//  godt
//
//  Created by Maciej Baginski on 26/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation

class RecipeDetailsPresenter {

    weak private var view: RecipeDetailsView?
    private var recipeViewData: RecipeViewData

    init(view: RecipeDetailsView, recipeViewData: RecipeViewData) {
        self.view = view
        self.recipeViewData = recipeViewData
    }

    func attachView(view: RecipeDetailsView) {
        self.view = view
    }

    func viewDidLoad() {
        view?.updateData()
    }

    func getRecipe() -> RecipeViewData {
        return recipeViewData
    }
}
