//
//  Ingredient.swift
//  godt
//
//  Created by Maciej Baginski on 23/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation

struct Ingredient {
    let amount: Int?
    let hint: String?
    let name: String?
    let symbol: String?
}
