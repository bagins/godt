//
//  Router.swift
//  godt
//
//  Created by Maciej Baginski on 26/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation
import UIKit

protocol Router {
    func prepare(for segue: UIStoryboardSegue, sender: Any?)
}

extension Router {
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    }
}
