//
//  RecipeTableViewCell.swift
//  godt
//
//  Created by Maciej Baginski on 23/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import UIKit
import Kingfisher

class RecipeTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var recipeImageView: UIImageView!

    func setupWithRecipeDetails(recipeDetails: RecipeViewData) {
        nameLabel.textColor = .godtRed
        backgroundColor = .godtBackground
        nameLabel.text = recipeDetails.title
        descriptionLabel.text = recipeDetails.description
        recipeImageView.kf.setImage(with: recipeDetails.imageUrl, placeholder: #imageLiteral(resourceName: "placeholder"), options: [.transition(.fade(1))])
    }

    func cancelDownloadTasks() {
        recipeImageView.kf.cancelDownloadTask()
    }
}
