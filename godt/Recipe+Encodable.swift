//
//  Recipe+NSCoder.swift
//  godt
//
//  Created by Maciej Baginski on 23/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation

extension Recipe: Encodable, Decodable {

    init(dictionary: [String: Any]) {
        title = dictionary["title"] as? String
        description = dictionary["description"] as? String
        ingredients = (dictionary["ingredients"] as? [[String: Any]])?.map {
            Ingredient(dictionary: $0)
        }
        if let imageUrlString = dictionary["imageUrl"] as? String {
            imageUrl = URL(string: imageUrlString)
        } else {
            imageUrl = nil
        }

    }

    func encode() -> [String: Any] {
        var result = [String: Any]()
        result["title"] = title
        result["description"] = description
        result["ingredients"] = ingredients?.map {
            $0.encode()
        }
        result["imageUrl"] = imageUrl?.absoluteString
        return result
    }
}
