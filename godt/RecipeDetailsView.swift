//
//  RecipeDetailsView.swift
//  godt
//
//  Created by Maciej Baginski on 26/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation

protocol RecipeDetailsView: class {
    func updateData()
}
