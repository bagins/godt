//
//  UIColor+Extensions.swift
//  godt
//
//  Created by Maciej Baginski on 26/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static var godtBackground: UIColor {
        return UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
    }
    static var godtRed: UIColor {
        return UIColor(red: 207/255, green: 67/255, blue: 42/255, alpha: 1)
    }
    static var godtBrown: UIColor {
        return UIColor(red: 89/255, green: 81/255, blue: 76/255, alpha: 1)
    }
}
