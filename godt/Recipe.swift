//
//  Recipe.swift
//  godt
//
//  Created by Maciej Baginski on 23/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation

struct Recipe {
    let title: String?
    let description: String?
    let ingredients: [Ingredient]?
    let imageUrl: URL?
}
