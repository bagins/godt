//
//  Recipe+JSON.swift
//  godt
//
//  Created by Maciej Baginski on 23/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation

extension Recipe {
    init?(JSON: Any) {
        guard let recipeJSON = JSON as? [String: Any] else {
            return nil
        }
        title = recipeJSON["title"] as? String
        description = recipeJSON["description"] as? String

        var recipeImageUrl: URL? = nil
        if let imagesJSON = recipeJSON["images"] as? [Any] {
            if let imageJSON = imagesJSON.first as? [String: Any] {
                if let url = imageJSON["url"] as? String {
                    recipeImageUrl = URL(string: url)
                }
            }
        }
        imageUrl = recipeImageUrl

        var recipeIngredients = [Ingredient]()

        if let ingredientsValue = recipeJSON["ingredients"] as? [Any] {
            if let elements = (ingredientsValue.first as? [String: Any])?["elements"] as? [Any] {
                for element in elements {
                    guard let ingredient = Ingredient(JSON: element) else {
                        continue
                    }
                    recipeIngredients.append(ingredient)
                }
            }
        }
        ingredients = recipeIngredients
    }
}
