//
//  Encodable.swift
//  godt
//
//  Created by Maciej Baginski on 23/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation

protocol Encodable {
    func encode() -> [String: Any]
}
