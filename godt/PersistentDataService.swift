//
//  PersistentDataService.swift
//  godt
//
//  Created by Maciej Baginski on 23/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation

protocol PersistentDataService {
    func archiveData(data: Any, fileName: String, completionBlock: (Error?) -> Void)
    func unarchiveData(fileName: String, completionBlock: (Error?, Any?) -> Void)
}

class PersistentDataServiceImpl: PersistentDataService {

    enum ArchiveError: Error {
        case failedToCreatePath
        case failedToArchive
        case failedToUnarchive
    }

    private let fileManager = FileManager.default
    private var documentDirectoryUrl: URL? {
        return fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
    }

    func archiveData(data: Any, fileName: String, completionBlock: (Error?) -> Void) {
        guard let path = documentDirectoryUrl?.appendingPathComponent(fileName).path else {
            completionBlock(ArchiveError.failedToCreatePath)
            return
        }
        let success = NSKeyedArchiver.archiveRootObject(data, toFile: path)
        if success {
            completionBlock(nil)
        } else {
            completionBlock(ArchiveError.failedToArchive)
        }
    }
    func unarchiveData(fileName: String, completionBlock: (Error?, Any?) -> Void) {
        guard let path = documentDirectoryUrl?.appendingPathComponent(fileName).path else {
            completionBlock(ArchiveError.failedToCreatePath, nil)
            return
        }
        if let data = NSKeyedUnarchiver.unarchiveObject(withFile: path) {
            completionBlock(nil, data)
        } else {
            completionBlock(ArchiveError.failedToUnarchive, nil)
        }
    }
}
