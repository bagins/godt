//
//  RecipeDetailsViewController.swift
//  godt
//
//  Created by Maciej Baginski on 23/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import UIKit
import Kingfisher

class RecipeDetailsViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var ingredientsLabel: UILabel!
    @IBOutlet weak var scrollViewContentView: UIView!

    var presenter: RecipeDetailsPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .godtBackground
        scrollView.backgroundColor = .godtBackground
        scrollViewContentView.backgroundColor = .godtBackground
        titleLabel.textColor = .godtRed
        presenter?.viewDidLoad()
    }
}

extension RecipeDetailsViewController: RecipeDetailsView {
    func updateData() {
        if let recipe = presenter?.getRecipe() {
            DispatchQueue.main.async {
                self.titleLabel.text = recipe.title
                self.descriptionLabel.text = recipe.description
                self.ingredientsLabel.text = recipe.ingredients
                self.imageView.kf.setImage(with: recipe.imageUrl, placeholder: #imageLiteral(resourceName: "placeholder"), options: [.transition(.fade(1))])
            }
        }
    }
}
