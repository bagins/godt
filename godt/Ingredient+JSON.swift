//
//  Ingredient+JSON.swift
//  godt
//
//  Created by Maciej Baginski on 23/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation

extension Ingredient {
    init?(JSON: Any) {
        guard let ingredientJSON = JSON as? [String: Any] else {
            return nil
        }
        amount = ingredientJSON["amount"] as? Int
        hint = ingredientJSON["hint"] as? String
        name = ingredientJSON["name"] as? String
        symbol = ingredientJSON["symbol"] as? String
    }
}
