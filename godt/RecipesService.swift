//
//  RecipesService.swift
//  godt
//
//  Created by Maciej Baginski on 23/12/2017.
//  Copyright © 2017 Maciej Baginski. All rights reserved.
//

import Foundation
import Alamofire

protocol RecipesService {
    func getRecipes(completion:@escaping (Error?, [Recipe]?) -> Void)
}

class RecipesServiceImpl: RecipesService {

    private let recipesUrl = "https://www.godt.no/api/getRecipesListDetailed?tags=&size=thumbnail-medium&ratio=1&limit=50&from=0"
    private let manager: Alamofire.SessionManager

    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 15
        manager = Alamofire.SessionManager(configuration: configuration)
    }

    func getRecipes(completion:@escaping (Error?, [Recipe]?) -> Void) {
        manager.request(recipesUrl, method: .get).validate().responseJSON { (response) in
            guard response.result.error == nil else {
                completion(response.result.error, nil)
                return
            }
            var recipes = [Recipe]()
            guard let JSON = response.result.value as? [Any] else {
                return
            }
            for item in JSON {
                guard let recipe = Recipe(JSON: item) else {
                    continue
                }
                recipes.append(recipe)
            }
            completion(nil, recipes)
        }
    }
}
